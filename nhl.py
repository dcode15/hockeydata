import utilities
from player import Skater, Goalie
import re


def get_unique_key(name, season_id):
    return re.sub('[^a-zA-Z]+', '', name).lower() + str(season_id)


def nhl_skaters_to_csv():
    skater_summary = utilities.get_json(
        'http://www.nhl.com/stats/rest/skaters?isAggregate=false&reportType=basic&isGame=false&reportName=skatersummary&sort=[{%22property%22:%22points%22,%22direction%22:%22DESC%22},{%22property%22:%22goals%22,%22direction%22:%22DESC%22},{%22property%22:%22assists%22,%22direction%22:%22DESC%22}]&cayenneExp=gameTypeId=2%20and%20seasonId%3E=19171918%20and%20seasonId%3C=20182019')
    skater_secondary = utilities.get_json(
        'http://www.nhl.com/stats/rest/skaters?isAggregate=false&reportType=basic&isGame=false&reportName=realtime&sort=[{%22property%22:%22hits%22,%22direction%22:%22DESC%22}]&cayenneExp=gameTypeId=2%20and%20seasonId%3E=19171918%20and%20seasonId%3C=20182019')
    skater_summary = skater_summary['data']
    skater_secondary = skater_secondary['data']

    skater_dict = {}
    for entry in skater_summary:
        key = get_unique_key(entry['playerName'], entry['seasonId'])
        skater_dict[key] = {'summary': entry}

    for entry in skater_secondary:
        key = get_unique_key(entry['playerName'], entry['seasonId'])
        skater_dict[key]['secondary'] = entry

    players = []
    for key, record in skater_dict.items():
        player = Skater()
        player.assists = record['summary']['assists']
        player.game_winning_goals = record['summary']['gameWinningGoals']
        player.games_played = record['summary']['gamesPlayed']
        player.goals = record['summary']['goals']
        player.ot_goals = record['summary']['otGoals']
        player.penalty_minutes = record['summary']['penaltyMinutes']
        player.birth_city = record['summary']['playerBirthCity']
        player.birth_country = record['summary']['playerBirthCountry']
        player.birth_date = record['summary']['playerBirthDate']
        player.birth_state = record['summary']['playerBirthStateProvince']
        player.draft_pick = record['summary']['playerDraftOverallPickNo']
        player.draft_round = record['summary']['playerDraftRoundNo']
        player.draft_year = record['summary']['playerDraftYear']
        player.first_name = record['summary']['playerFirstName']
        player.height = record['summary']['playerHeight']
        player.nhl_id = record['summary']['playerId']
        player.hall_of_famer = bool(record['summary']['playerInHockeyHof'])
        player.last_name = record['summary']['playerLastName']
        player.position = record['summary']['playerPositionCode']
        player.handedness = record['summary']['playerShootsCatches']
        player.team = record['summary']['playerTeamsPlayedFor']
        player.weight = record['summary']['playerWeight']
        player.plus_minus = record['summary']['plusMinus']
        player.powerplay_goals = record['summary']['ppGoals']
        player.powerplay_assists = record['summary']['ppPoints'] - record['summary']['ppGoals'] if record['summary']['ppPoints'] is not None else None
        player.season = str(record['summary']['seasonId'])[:4] + '-' + str(record['summary']['seasonId'])[4:]
        player.shorthanded_goals = record['summary']['shGoals']
        player.shorthanded_assists = record['summary']['shPoints'] - record['summary']['shGoals'] if record['summary']['shPoints'] is not None else None
        player.shifts_per_game = record['summary']['shiftsPerGame']
        player.shots = record['summary']['shots']
        player.time_on_ice_per_game = record['summary']['timeOnIcePerGame']
        player.blocked_shots = record['secondary']['blockedShots']
        player.faceoffs_lost = record['secondary']['faceoffsLost']
        player.faceoffs_won = record['secondary']['faceoffsWon']
        player.giveaways = record['secondary']['giveaways']
        player.hits = record['secondary']['hits']
        player.missed_shots = record['secondary']['missedShots']
        player.takeaways = record['secondary']['takeaways']
        player.is_projection = False
        player.data_source = 'NHL'
        players.append(player)

    utilities.write_players_to_csv(players, Skater.get_header_list(), 'nhl-skaters')
