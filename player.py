from abc import ABC, abstractmethod


class Player(ABC):
    first_name = None
    last_name = None
    team = None
    position = None
    games_played = None
    is_projection = None
    season = None
    birth_city = None
    birth_country = None
    birth_date = None
    birth_state = None
    draft_pick = None
    draft_round = None
    draft_year = None
    height = None
    nhl_id = None
    hall_of_famer = None
    handedness = None
    weight = None
    data_source = None

    @abstractmethod
    def to_list(self):
        pass

    @staticmethod
    def get_header_list():
        pass

    def is_goalie(self):
        return self.position == 'G'

    def get_common_attribute_values(self):
        return [self.first_name, self.last_name, self.team, self.position, self.games_played, self.is_projection,
                self.season, self.birth_city, self.birth_country, self.birth_date, self.birth_state, self.draft_pick,
                self.draft_round, self.draft_year, self.height, self.nhl_id, self.hall_of_famer, self.handedness,
                self.weight, self.data_source]

    @staticmethod
    def get_common_attribute_labels():
        return ['First_Name', 'Last_Name', 'Team', 'Position', 'Games_Played', 'Projection',
                'Season', 'Birth_City', 'Birth_Country', 'Birth_Date', 'Birth_State', 'Draft_Pick',
                'Draft_Round', 'Draft_Year', 'Height', 'NHL_ID', 'Hall_Of_Famer', 'Handedness',
                'Weight', 'Data_Source']


class Skater(Player):
    goals = None
    assists = None
    shots = None
    penalty_minutes = None
    powerplay_goals = None
    powerplay_assists = None
    hits = None
    blocked_shots = None
    game_winning_goals = None
    ot_goals = None
    plus_minus = None
    shorthanded_goals = None
    shorthanded_assists = None
    shifts_per_game = None
    time_on_ice_per_game = None
    faceoffs_won = None
    faceoffs_lost = None
    giveaways = None
    missed_shots = None
    takeaways = None

    @staticmethod
    def get_header_list():
        return Player.get_common_attribute_labels() + ['Goals', 'Assists', 'Penalty_Minutes', 'Powerplay_Goals',
                                                       'Powerplay_Assists', 'Hits', 'Blocked_Shots',
                                                       'Game_Winning_Goals', 'Overtime_Goals', 'Plus_Minus',
                                                       'Shorthanded_Goals', 'Shorthanded_Assists', 'Shifts_Per_Game',
                                                       'Time_On_Ice_Per_Game', 'Faceoffs_Won', 'Faceoffs_Lost',
                                                       'Giveaways', 'Missed_Shots', 'Takeaways']

    def to_list(self):
        return self.get_common_attribute_values() + [self.goals, self.assists, self.penalty_minutes,
                                                     self.powerplay_goals, self.powerplay_assists, self.hits,
                                                     self.blocked_shots,
                                                     self.game_winning_goals, self.ot_goals, self.plus_minus,
                                                     self.shorthanded_goals,
                                                     self.shorthanded_assists, self.shifts_per_game,
                                                     self.time_on_ice_per_game, self.faceoffs_won,
                                                     self.faceoffs_lost, self.giveaways, self.missed_shots,
                                                     self.takeaways]


class Goalie(Player):
    position = 'G'
    wins = None
    goals_against_average = None
    saves = None
    goals_against = None
    shutouts = None

    @staticmethod
    def get_header_list():
        return ['Name', 'Team', 'Position', 'Year', 'Projection', 'GP', 'W', 'GAA', 'SV', 'GA', 'SO']

    def to_list(self):
        return [self.name, self.team, self.position, self.year, self.is_projection, self.games_played, self.wins,
                self.goals_against_average, self.saves, self.goals_against, self.shutouts]
