from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup

import csv


def get_raw_html(url):
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)


def log(e):
    print(e)


def get_processed_html(url):
    return BeautifulSoup(get_raw_html(url), 'html.parser')


def get_processed_local_html(path):
    return BeautifulSoup(open(path), 'html.parser')


def get_json(url):
    return get(url).json()


def write_players_to_csv(player_list, header_list, csv_name):
    with open('output/' + csv_name + '.csv', mode='w') as output_file:
        player_writer = csv.writer(output_file, delimiter=',', quotechar='"', lineterminator='\n')

        if header_list is not None:
            player_writer.writerow(header_list)

        for player in player_list:
            player_writer.writerow(player.to_list())
